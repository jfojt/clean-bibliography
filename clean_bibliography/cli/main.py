from clean_bibliography import __version__


class CLIError(Exception):
    """Error for CLI commands.

    A subcommand may raise this.  The message will be forwarded to
    the error() method of the argument parser."""


commands = [
    ('autogen', 'data_manager.cli.autogen'),
    ('tree', 'data_manager.cli.tree'),
]


def main(prog='clean-bibr', description='clean bibtex bibliography',
         version=__version__, commands=commands, hook=None, args=None):
    from clean_bibliography.process_bib import main as process_main
    process_main()
