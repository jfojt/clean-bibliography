import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter

from clean_bibliography import abbrev_listfile, abbrev_dots_listfile

try:
    from colorama import init
    init()
    from colorama import Fore, Style

    def highlight(s, color='RED'):
        return getattr(Fore, color.upper()) + str(s) + Style.RESET_ALL

except ModuleNotFoundError:
    def highlight(s):
        return s


class LookupDict(dict):

    def __init__(self, **kwargs):
        kwargs = {self._key(key): self._value(value)
                  for key, value in kwargs.items()}
        super().__init__(**kwargs)

    def __getitem__(self, key):
        sup = super(LookupDict, self)
        _key = self._key(key)
        if sup.__contains__(_key):
            value = sup.__getitem__(_key)
            return value
        else:
            if _key[:4] == 'the ':
                return self[key[4:]]
            print(f'Warning: No value found for {highlight(key)}')
            return key

    def __setitem__(self, key, value):
        super(LookupDict, self).__setitem__(self._key(key),
                                            self._value(value))

    def _key(self, key):
        return key.rstrip(' ').replace(r'\&', '&').replace(r'\"', '').lower()

    def _value(self, value):
        return value.lstrip(' ')


def load_abbreviations(with_dots=True):
    if with_dots:
        fname = abbrev_dots_listfile
    else:
        fname = abbrev_listfile

    abbrev_to_full = LookupDict()
    full_to_abbrev = LookupDict()
    with open(fname) as fp:
        for line in fp:
            line = line.rstrip('\n')
            try:
                full, abbrev = line.split('=')
                if abbrev in abbrev_to_full:
                    msg = f'{abbrev_to_full[abbrev]} -> {full} = {abbrev}'
                    print(f'Duplicate line {highlight(msg, "YELLOW")}')
                abbrev_to_full[abbrev] = full
                if full in full_to_abbrev:
                    msg = f'{full} = {full_to_abbrev[full]} -> {abbrev}'
                    print(f'Duplicate line {highlight(msg, "YELLOW")}')
                full_to_abbrev[full] = abbrev
            except ValueError:  # Not enough values to unpack
                print(f'Skipping line {highlight(line, "YELLOW")}')

    # Extra keys
    for full, abbrev in [
            ('Nature Catalysis', 'Nat. Catal.'),
            ('Journal of Physics: Condensed Matter', 'J. Phys. Condens. Matter'),
            ('Journal of Molecular Structure: THEOCHEM', 'J. Mol. Struc. THEOCHEM'),
            ('Proceedings of the National Academy of Sciences', 'Proc. Natl. Acad. Sci. U.S.A. '),
            ('ACS Applied Nano Materials', 'ACS Appl. Nano Mater.'),
            ('ACS Energy Letters', 'ACS Energy Lett.'),
            ('Communications Physics', 'Commun. Phys.'),
            ('Computing in Science Engineering', 'Comput. Sci. Eng.'),
            ]:
        abbrev_to_full[abbrev] = full
        full_to_abbrev[full] = abbrev

    return abbrev_to_full, full_to_abbrev


def strip_month(contents):
    """
    For some reason the month field is really problematic. Maybe because the
    month isn't surrounded by brackets {} ?
    """
    import re

    return re.sub(r'month\s+=\s+[A-z]{3},\s*', '', contents)


def parse_bibtex(infile, outfile, full_to_abbrev=None, strip_fields=[]):
    # Load bibfile
    parser = BibTexParser()
    parser.ignore_nonstandard_types = False
    parser.homogenize_fields = True
    parser.common_strings = False
    with open(infile) as bibtex_file:
        bibtex_contents = strip_month(bibtex_file.read())
    bib_database = bibtexparser.loads(bibtex_contents, parser)

    # Abbreviate journal names, remove superfluous fields
    replaced = dict()
    if full_to_abbrev is not None:
        for entry in bib_database.entries_dict.values():
            if entry['ENTRYTYPE'] != 'article':
                continue
            try:
                journal = entry['journal']
                abbrev = full_to_abbrev[journal]
                entry['journal'] = abbrev
                if journal != abbrev:
                    replaced[journal] = abbrev
            except KeyError:
                print(f'No journal for {highlight(entry, "BLUE")}')

    for key in strip_fields:
        for entry in bib_database.entries_dict.values():
            entry.pop(key, None)

    writer = BibTexWriter()
    writer.contents = ['entries']
    writer.indent = '  '
    writer.order_entries_by = ('ENTRYTYPE', 'author', 'year')

    if full_to_abbrev is not None:
        print('Replaced')
        w = max([len(journal) for journal in replaced.keys()])
        for journal, abbrev in replaced.items():
            pad = (w - len(journal))*' '
            print(f'{journal}{pad} ->', highlight(abbrev, 'green'))
    # Write
    with open(outfile, 'w') as bibtex_file:
        bibtexparser.dump(bib_database, bibtex_file, writer)


def main():
    import argparse

    parser = argparse.ArgumentParser(description="Parse bibtex bibliography.")
    parser.add_argument('bibfile', help='Bibliography file')
    parser.add_argument('-o', '--output',
                        help='Output file (default overwrite)')
    parser.add_argument('-a', '--abbrev',
                        action='store_true',
                        help='Abbreviate journal names')
    parser.add_argument('-s', '--strip',
                        action='store_true',
                        help='Strip unnecessary fields')

    args = parser.parse_args()

    if args.abbrev:
        _, full_to_abbrev = load_abbreviations()
    else:
        full_to_abbrev = None

    infile = args.bibfile
    outfile = args.output
    if outfile is None:
        outfile = infile

    strip_fields = []
    if args.strip:
        strip_fields = ['issn', 'file', 'abstract', 'copyright', 'urldate']
    parse_bibtex(infile, outfile, full_to_abbrev, strip_fields=strip_fields)


if __name__ == "__main__":
    main()
