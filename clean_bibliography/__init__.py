__version__ = '0.1'

import os

location = os.path.dirname(os.path.realpath(__file__))
abbrev_dots_listfile = os.path.join(location, 'assets', 'jabref_wos_abbrev_dots.txt')
abbrev_listfile = os.path.join(location, 'assets', 'jabref_wos_abbrev.txt')
