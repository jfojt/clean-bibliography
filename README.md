# clean-bibliography

Clean your bibtex bibliography. For example abbreviate all journal names

## Install

Where . is the path to this repo
```
pip install -e .
```

## Process

```
clean-bib lit.bib --output clean-lit.bib
```
