from setuptools import setup

setup(
    name="Clean bibliography",
    version="0.1",
    description="Clean your bibtex bibliography",
    url=None,
    author="Jakub Fojt",
    author_email="jakub.fojt96@gmail.com",
    platforms=["linux"],
    packages=["clean_bibliography"],
    package_data={'clean_bibliography': ['assets/*']},
    long_description="""Clean your bibtex bibliography""",
    python_requires=">=3.6",
    install_requires=["bibtexparser", "colorama"],
    entry_points={'console_scripts': ['clean-bib=clean_bibliography.cli.main:main']},
)
